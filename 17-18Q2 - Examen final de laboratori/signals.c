#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

int comptador = 0;
int nsecs = 0;

//usage function
void usage()
{
  char buff[256];
  sprintf(buff, "Usage: signals N\n \
  Aquest programa fa coses.\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void error_and_exit(char *msg, int exit_status)
{
  perror(msg);
  exit(exit_status);
}

void trata_signals(int s) {
  char buff[256];
  if (s == SIGUSR1) {
    sprintf(buff, "SIGUSR1 rebut\n");
    write(1, buff, strlen(buff));
    if (++comptador == 3) exit(0);
  }
  else if (s == SIGUSR2) {
    sprintf(buff, "SIGUSR2 rebut\n");
    write(1, buff, strlen(buff));
    exit(0);
  }
  else if (s == SIGALRM) {
    sprintf(buff, "Ha passat 1 segon\n");
    write(1, buff, strlen(buff));
    ++nsecs;
  }
}

int main(int argc, char *argv[]) {
  char buff[256];
  if (argc != 2) usage();

  sigset_t mask;
  sigfillset(&mask);
  sigprocmask(SIG_BLOCK, &mask, NULL);

  struct sigaction sa;
  sa.sa_handler = &trata_signals;
  sa.sa_flags = SA_RESTART;
  sigfillset(&sa.sa_mask);

  sigaction(SIGUSR1, &sa, NULL);
  sigaction(SIGUSR2, &sa, NULL);
  sigaction(SIGALRM, &sa, NULL);

  int n = atoi(argv[1]);

  sigfillset(&mask);
  sigdelset(&mask, SIGUSR1);
  sigdelset(&mask, SIGUSR2);
  sigdelset(&mask, SIGINT);
  sigdelset(&mask, SIGALRM);
  while (nsecs < n) {
    alarm(1);
    sigsuspend(&mask);
  }
}
