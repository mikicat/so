#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void usage()
{
  char buff[256];
  sprintf(buff, "Usage: exam1 fitxer1 fitxer2\n \
  Aquest programa executa desc_ok i en guarda l'output\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void error_and_exit(char *msg) {
  perror(msg);
  exit(1);
}

int main(int argc, char *argv[]) {
  if (argc != 3) usage();
  int file1 = open(argv[1], O_RDONLY);
  int file2 = open(argv[2], O_WRONLY|O_CREAT|O_TRUNC, 0640);
  int ret = fork();
  if (ret < 0) error_and_exit("Error al fork\n");
  if (ret == 0) {
    if (dup2(file1, 0) < 0) error_and_exit("Error al dup2\n");
    if (dup2(file2, 1) < 0) error_and_exit("Error al dup2\n");
    execlp("./desc_ok", "desc_ok", (char *)NULL);
    error_and_exit("Error al execlp\n");
  }
  waitpid(ret, NULL, 0);
  close(file1);
  close(file2);
}
