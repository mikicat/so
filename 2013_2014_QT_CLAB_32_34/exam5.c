#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>

void error_and_exit(char *msg, int exit_status)
{
    perror(msg);
    exit(exit_status);
}

void usage() {
  char buff[256];
  sprintf(buff, "Usage: exam2 n(num. de processos)\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void trata_signals(int s) {
  char buff[256];
  if (s == SIGUSR1) {
    sprintf(buff, "Procés %d ha rebut SIGUSR1\n", getpid());
    write(1, buff, strlen(buff));
  }
}

int main(int argc, char *argv[]) {
  char buff[256];
  if (argc != 2) usage();
  int N = atoi(argv[1]);

  struct sigaction sa;
  sa.sa_flags = SA_RESTART;
  sa.sa_handler = &trata_signals;
  sigfillset(&sa.sa_mask);
  if(sigaction(SIGUSR1, &sa, NULL) < 0) error_and_exit("error en sigaction\n", 1);

  for (int i = 0; i < N; ++i) {
    int ppid = getppid();
    int pid = fork();
    if (pid < 0) error_and_exit("Error en el fork\n", 1);
    if (pid == 0) {
      sprintf(buff, "Procés %d creat\n", getpid());
      write(1, buff, strlen(buff));

      if (i == N-1) {
        if(sigaction(SIGALRM, &sa, NULL) < 0) error_and_exit("error en sigaction\n", 1);
        sigset_t mask;
        alarm(5);
        sigfillset(&mask);
        sigdelset(&mask, SIGALRM);
        sigdelset(&mask, SIGINT);
        sigsuspend(&mask);
      }
      if (i != 0) kill(ppid, SIGUSR1);
      else kill(getppid(), SIGUSR1);
    }
    else {
      if ((pid = waitpid(-1, NULL, 0)) < 0) error_and_exit("waitpid", 1);

      sprintf(buff, "Procés %d finalitzat\n", pid);
      write(1, buff, strlen(buff));
      exit(0);
    }
  }
}
