#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>

void error_and_exit(char *msg, int exit_status)
{
    perror(msg);
    exit(exit_status);
}

void usage() {
  char buff[256];
  sprintf(buff, "Usage: exam3 n(num. de processos)\n");
  write(1, buff, strlen(buff));
  exit(0);
}



int main(int argc, char *argv[]) {
  char buff[256];
  if (argc != 2) usage();
  int N = atoi(argv[1]);
  for (int i = 0; i < N; ++i) {
    int pid = fork();
    if (pid < 0) error_and_exit("Error en el fork\n", 1);
    if (pid == 0) {
      sprintf(buff, "Procés %d creat\n", getpid());
      write(1, buff, strlen(buff));
    }
    else {
      waitpid(-1, NULL, 0);
      sprintf(buff, "Procés %d finalitzat\n", pid);
      write(1, buff, strlen(buff));
      exit(0);
    }
  }
  if (execlp("sleep", "sleep", "5", (char *)NULL) < 0) error_and_exit("Error en el execlp.\n",1);

}
