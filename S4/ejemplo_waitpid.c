#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>

int pids[10];
void error_y_exit(char *msg,int exit_status)
{
    perror(msg);
    exit(exit_status);
}

void trata_alarma(int s)
{
  for (int i = 0; i < sizeof(pids); ++i) {
    if (pids[i] != 0) kill(pids[i], SIGKILL);
  }
}

int main(int argc,char *argv[])
{

    int pid,res;
    char buff[256];
    int contador = 0;
    int hijos=0;
    struct sigaction sa;
    sigset_t mask;

    /* Evitamos recibir el SIGALRM fuera del sigsuspend */

    sigemptyset(&mask);
    sigaddset(&mask,SIGALRM);
    sigprocmask(SIG_BLOCK,&mask, NULL);

    for (hijos=0;hijos<10;hijos++)
    {
    	sprintf(buff, "Creando el hijo numero %d\n", hijos);
    	write(1, buff, strlen(buff));

    	pid=fork();
    	if (pid==0) /* Esta linea la ejecutan tanto el padre como el hijo */
    	{
        pids[hijos] = getpid();


  	    /* Escribe aqui el codigo del proceso hijo */
    	    sprintf(buff,"Hola, soy %d\n",getpid());
  	    write(1, buff, strlen(buff));

  	    alarm(1);
  	    sigfillset(&mask);
              sigdelset(&mask, SIGALRM);
              sigdelset(&mask, SIGINT);
              sigsuspend(&mask);

  	    /* Termina su ejecución */
  	    exit(0);
    	} else if (pid<0)
    	{
    	    /* Se ha producido un error */
      	    error_y_exit("Error en el fork", 1);
    	}
    }
    /* Esperamos que acaben los hijos */
    while (hijos > 0)
    {
      sa.sa_handler = &trata_alarma;
      sa.sa_flags = 0;
      sigfillset(&sa.sa_mask);
      if (sigaction(SIGALRM, &sa, NULL) < 0) error_y_exit("sigaction", 1);
      alarm(2);
      sigfillset(&mask);
      sigdelset(&mask, SIGALRM);
      sigdelset(&mask, SIGINT);

    	pid=waitpid(-1,&res,0);
      int ret = alarm(0);

      for (int i = 0; i < hijos; ++i) {
        if (pids[i] == pid) pids[i] = 0;
      }
      sprintf(buff, "PID %d: Faltaven %d segons perque l'alarma saltes.\n", pid, ret);
      write(1, buff, strlen(buff));


    	sprintf(buff,"Termina el proceso %d\n",pid);
    	write(1, buff, strlen(buff));
      if (WIFEXITED(res)) {
        sprintf(buff, "Exit code: %d\n", WEXITSTATUS(res));
        write(1, buff, strlen(buff));
      } else {
        sprintf(buff, "Terminado por signal: %d", WTERMSIG(res));
      }

    	hijos --;
    	contador++;
    }
    sprintf(buff,"Valor del contador %d\n", contador);
    write(1, buff, strlen(buff));
    return 0;
}
