#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>

int seg = 0;

void error_y_exit(char *msg,int exit_status)
{
    perror(msg);
    exit(exit_status);
}


void trata_signals(int s) {
  if (s == SIGALRM) {
    ++seg;
  }
  else if (s == SIGUSR1) {
    char buff[256];
    sprintf(buff, "Inicio tratamiento\n");
    write(1, buff, strlen(buff));
    kill(getpid(), SIGINT);
    sprintf(buff, "Fin tratamiento\n");
    write(1, buff, strlen(buff));
  }
  else if (s == SIGUSR2) {
    char buff[256];
    sprintf(buff, "El valor actual del comptador es %d segons.\n", seg);
    write(1, buff, strlen(buff));
  }
}

int main(int argc,char *argv[]) {
  struct sigaction sa;
  sigset_t mask;
  sigemptyset(&mask);
  sigaddset(&mask, SIGALRM);
  sigaddset(&mask, SIGUSR1);
  sigaddset(&mask, SIGUSR2);
  sigprocmask(SIG_BLOCK, &mask, NULL);

  sa.sa_flags = SA_RESTART;
  sa.sa_handler = &trata_signals;
  sigfillset(&sa.sa_mask);
  sigdelset(&sa.sa_mask, SIGINT);

  while (1) {
    if (sigaction(SIGALRM, &sa, NULL) < 0) error_y_exit("sigalrm", 1);
    if (sigaction(SIGUSR1, &sa, NULL) < 0) error_y_exit("sigusr1", 1);
    if (sigaction(SIGUSR2, &sa, NULL) < 0) error_y_exit("sigusr2", 1);

    alarm(1);
    sigfillset(&mask);
    sigdelset(&mask, SIGALRM);
    sigdelset(&mask, SIGUSR1);
    sigdelset(&mask, SIGUSR2);
    sigdelset(&mask, SIGINT);
    sigsuspend(&mask);
  }
}
