#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void error_y_exit(char* msg) {
	perror(msg);
	exit(1);
}

int main(int argc, char **argv) {
	char buff[64];
  int fd  = open("salida.txt", O_RDWR);
	if (fd < 0) error_y_exit("Error en el open");
	lseek(fd, -1, SEEK_END); // Repositions file offset
	int ret = read(fd, buff, sizeof(char)); // Llegeix l'últim caràcter
	lseek(fd, -1, SEEK_END);
	write(fd, "X", sizeof(char)); // Escriu en penúltima posició
	write(fd, buff, ret);
}
