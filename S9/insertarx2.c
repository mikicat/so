#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>

void usage() {
  char buff[256];
  sprintf(buff, "Usage: ./insertarx2 file_name posicio\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void error_and_exit(char *msg) {
  perror(msg);
  exit(1);
}

int main(int argc, char **argv) {
  if (argc != 2) usage();
	char buff[64];
  int pos = atoi(argv[2]);
  int fd  = open(argv[1], O_RDWR);
	if (fd < 0) error_and_exit("Error en el open");
  int off = -2;
	lseek(fd, -1, SEEK_END);
  char c;
  while(read(fd, &c, sizeof(char)) > 0) {
    write(fd, &c, sizeof(char));
    if (lseek(fd, off, SEEK_END) < 0) return;
    --off;
    if (lseek(fd, 0, SEEK_CUR) == pos) {
      write(fd, 'X', sizeof(char));
      return;
    }
  }

}
