#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void usage() {
  char buff[256];
  sprintf(buff, "Usage: ./invirtiendo_fichero file_name\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void error_and_exit(char *msg) {
  perror(msg);
  exit(1);
}

int main(int argc, char *argv[]) {
  if (argc != 2) usage();
  int fd = open(argv[1], O_RDWR);
  if (fd < 0) error_and_exit("Could not open file");
  char *name;
  sprintf(name, "%s.inv", argv[1]);
  int inv_fd = creat(name, 0660);
  if (inv_fd < 0) error_and_exit("Error in creating new file");
  int init = lseek(fd, 0, SEEK_SET);
  int end = lseek(fd, 0, SEEK_END);
  char c;
  while (end >= init) {
    lseek(inv_fd, 0, SEEK_END);
    read(fd, &c, sizeof(char));
    write(inv_fd, &c, sizeof(char));
    --end;
    lseek(fd, end, SEEK_SET);
  }

}
