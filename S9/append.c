#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void usage() {
  char buff[256];
  sprintf(buff, "Usage: ./append file_name\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void error_and_exit(char *msg) {
  perror(msg);
  exit(1);
}

int main(int argc, char *argv[]) {
  if (argc != 2) usage();
  int fd = open(argv[1], O_RDWR);
  if (fd < 0) error_and_exit("Could not open file");
  int init = 0;
  int end;
  if ((end = lseek(fd, 0, SEEK_END)) < 0) error_and_exit("Lseek error");
  char c;
  lseek(fd, init, SEEK_SET);
  while ((init < end) && (read(fd, &c, sizeof(char)) > 0)) {
    lseek(fd, 0, SEEK_END);
    write(fd, &c, sizeof(char));
    ++init;
    lseek(fd, init, SEEK_SET);
  }
}
