#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
int fin_alarm = 0;
void error_y_exit(char *msg,int exit_status)
{
    perror(msg);
    exit(exit_status);
}

void usage() {
  char buff[256];
  sprintf(buff, "usage: signals N\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void trata_signal(int s) {
  if (s == SIGALRM) {
    fin_alarm = 1;
  }
}

int main(int argc, char *argv[]) {
  char buff[256];
  if (argc != 2) usage();
  sigset_t mask;
  sigemptyset(&mask);
  sigaddset(&mask, SIGUSR1);
  sigaddset(&mask, SIGALRM);
  sigprocmask(SIG_BLOCK, &mask, NULL);

  struct sigaction sa;
  sa.sa_handler = &trata_signal;
  sigfillset(&sa.sa_mask);
  sa.sa_flags = SA_RESTART;

  for (int i = 0; i < atoi(argv[1]); ++i) {
    int pid = fork();
    if (pid < 0) error_y_exit("Error en el fork.\n", 1);
    else if (pid == 0) {
      if (sigaction(SIGALRM, &sa, NULL) < 0) error_y_exit("sigaction", 1);
      sprintf(buff, "PID Hijo: %d\n", getpid());
      write(1, buff, strlen(buff));
      sigdelset(&mask, SIGUSR1);
      sigprocmask(SIG_UNBLOCK, &mask, NULL);
      alarm(1);
      while (!fin_alarm);
      kill(getppid(), SIGUSR1);
      exit(0);
    }
    else {
      if (sigaction(SIGUSR1, &sa, NULL) < 0) error_y_exit("sigaction", 1);
      sigfillset(&mask);
      sigdelset(&mask, SIGUSR1);
      sigdelset(&mask, SIGINT);
      sigsuspend(&mask);
    }
  }
}
