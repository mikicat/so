#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>

void error_y_exit(char *msg,int exit_status)
{
    perror(msg);
    exit(exit_status);
}

void usage() {
  char buff[256];
  sprintf(buff, "usage: status PID1 [PID2.. PIDn]\n");
  write(1, buff, strlen(buff));
  exit(0);
}

int main(int argc, char *argv[]) {
  char buff[256];
  if (argc < 2) usage();
  for (int i = 1; i < argc; ++i) {
    int pid = fork();
    if (pid < 0) error_y_exit("Error en el pid\n", 1);
    else if (pid == 0) {
      sprintf(buff, "/proc/%s/status", argv[i]);
      if (execlp("grep", "grep", "State", buff, (char *)NULL) < 0) error_y_exit("Error en execlp.\n", 1);
      exit(0);
    }
  }
  while(waitpid(-1, NULL, 0) > 0);
}
