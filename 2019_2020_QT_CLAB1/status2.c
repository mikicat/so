#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>



void error_y_exit(char *msg,int exit_status)
{
    perror(msg);
    exit(exit_status);
}

void usage() {
  char buff[256];
  sprintf(buff, "usage: status PID1 [PID2.. PIDn]\n");
  write(1, buff, strlen(buff));
  exit(0);
}

int main(int argc, char *argv[]) {
  int pids[10] = {};
  char buff[256];
  if (argc < 2) usage();
  for (int i = 1; i < argc; ++i) {
    int pid = fork();
    if (pid < 0) error_y_exit("Error en el pid\n", 1);
    else if (pid == 0) {
      sprintf(buff, "/proc/%s/status", argv[i]);
      if (execlp("grep", "grep", "State", buff, (char *)NULL) < 0) error_y_exit("Error en execlp.\n", 1);

    }
    else pids[i-1] = pid;
  }
  int exitcode;
  int pid;
  while((pid = waitpid(-1, &exitcode, 0)) > 0) {
    if (WIFEXITED(exitcode)) {
      if (WEXITSTATUS(exitcode) == 0) {
        for (int i = 0; i < 10; ++i) {
          if (pids[i] == pid) pids[i] = 0;
        }
      } else {
        for (int i = 0; i < 10; ++i) {
          if (pids[i] == pid) {
            sprintf(buff, "Proceso %s no existe.\n", argv[i+1]);
            write(1, buff, strlen(buff));
          }
          if (pids[i] > 0) kill(pids[i], SIGKILL);
        }
      }
    }
  }
}
