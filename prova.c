#include <unistd.h>
#include <stdio.h>

int main() {
  int fd[2];
  char c;
  pipe(fd);
  int pid = fork();
  if (pid == 0) {
    close(fd[0]);
    int i = 0;
    while (read(0, &c, 1) > 0) {
      write(fd[1], &c, 1);
    }
    close(fd[1]);
  }
  else {
    close(fd[1]);
    while (read(fd[0], &c, 1) > 0) {
      write(1, &c, 1);
    }
    close(fd[0]);
  }
}
