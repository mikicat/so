#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
//usage function
void usage()
{
  char buff[256];
  sprintf(buff, "Usage: gestor N (on N es parell)\n \
  Crea N processos.\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void error_and_exit(char *msg) {
  perror(msg);
  exit(1);
}

int pos(int *pids, int pid_mort, int n) {
  for (int i = 0; i < n; ++i) {
    if (pid_mort == pids[i]) return i;
  }
  return -1;
}

int main(int argc, char *argv[]) {
  if (argc != 2) usage();
  int n = atoi(argv[1]);
  int *pids = malloc(sizeof(int)*n);

  for (int i = 0; i < n; ++i) {
    int ret = fork();
    if (ret < 0) error_and_exit("Error en el fork\n");
    if (ret == 0) {
      if (i%2 == 0) {
        dup2(1, 8);
        execlp("./alfa", "alfa", (char *)NULL);
      }
      else {
        int popipe_r = open("./POPIPE", O_RDONLY);
        dup2(popipe_r, 7);
        close(popipe_r);
        char buff[8];
        sprintf(buff, "%d", pids[i-1]);
        execlp("./omega", "omega", buff, (char *)NULL);
      }
    }
    else {
      pids[i] = ret;
    }
  }

  int popipe_w = open("./POPIPE", O_WRONLY);
  char c;
  while(read(0, &c, 1) > 0) {
    if (c != 'q') {
      write(popipe_w, "z", 1);
    }
    else {
      for (int i = 0; i < n/2; ++i) {
        write(popipe_w, "Z", 1);
        kill(pids[i], SIGUSR2);
      }
    }
  }
  int pid_mort, exit_status;
  write(1, "A", 1);
  int fd = open("aes.int", O_WRONLY);
  while((pid_mort = waitpid(-1, &exit_status, 0)) > 0) {
    int i = pos(pids, pid_mort, n);
    if (i%2 == 0) {
      lseek(fd, -1, SEEK_END);
      char *text_;
      sprintf(text_, "\n%d", WEXITSTATUS(exit_status));
      write(fd, text_, strlen(text_));
    }
  }
  close(fd);
  close(popipe_w);
  free(pids);
}
