#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

//usage function
void usage()
{
  char buff[256];
  sprintf(buff, "Usage: alfa\n \
  Espera a que reciba un SIGUSR1 o un SIGUSR2.\n");
  write(1, buff, strlen(buff));
  exit(0);
}

int n = 0;

void signal_handler(int s) {
  if (s == SIGUSR1) {
    write(8, "a", 1);
    ++n;
    return;
  }
  if (s == SIGUSR2) {
    exit(n%255);
  }
}

int main(int argc, char *argv[]) {
  if (argc != 1) usage();

  sigset_t mask;
  sigemptyset(&mask);
  sigaddset(&mask, SIGUSR1);
  sigaddset(&mask, SIGUSR2);
  sigprocmask(SIG_BLOCK, &mask, NULL);

  struct sigaction sa;
  sa.sa_handler = &signal_handler;
  sa.sa_flags = SA_RESTART;
  sigfillset(&sa.sa_mask);
  sigaction(SIGUSR1, &sa, NULL);
  sigaction(SIGUSR2, &sa, NULL);

  sigfillset(&mask);
  sigdelset(&mask, SIGUSR1);
  sigdelset(&mask, SIGUSR2);
  sigdelset(&mask, SIGINT); // Bona praxi
  while (1) {
    sigsuspend(&mask);
  }
}
