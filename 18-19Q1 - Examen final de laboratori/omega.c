#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

//usage function
void usage()
{
  char buff[256];
  sprintf(buff, "Usage: omega PID\n \
  Llegeix el canal 7 i si arriba z envia SIGUSR1 al proces PID.\n");
  write(1, buff, strlen(buff));
  exit(0);
}

int main(int argc, char *argv[]) {
  if (argc != 2) usage();
  char c;
  while (read(7, &c, 1) > 0) {
    if (c == 'z') kill(atoi(argv[1]), SIGUSR1);
    else if (c == 'Z') exit(0);
  }
}
