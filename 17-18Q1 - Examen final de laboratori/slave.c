#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void usage()
{
  char buff[256];
  sprintf(buff, "Usage: slave fitxer\n \
  XD lol\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void error_and_exit(char *msg) {
  perror(msg);
  exit(1);
}

int main(int argc, char *argv[]) {
  if (argc != 2) usage();
  int fd = open(argv[1], O_RDONLY);
  if (fd < 0) error_and_exit("Error al open\n");
  int size = lseek(fd, 0, SEEK_END);
  if (size < 0) error_and_exit("Error al lseek\n");
  int *buffer = sbrk(size);
  lseek(fd, 0, SEEK_SET);
  if (read(fd, buffer, size) < 0) error_and_exit("Error al read\n");
  write(1, buffer, size);
  sbrk(-size);
  exit(0);
}
