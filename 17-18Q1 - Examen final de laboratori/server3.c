#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

int max;
int fills_vius = 0;

void usage()
{
  char buff[256];
  sprintf(buff, "Usage: server1\n \
  XD lol\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void error_and_exit(char *msg) {
  perror(msg);
  exit(1);
}

void trata_signals(int s) {
  if (s == SIGHUP) {
    char buff[25];
    sprintf(buff, "Fills vius: %d\n", fills_vius);
    write(2, buff, strlen(buff));
  }
}

int main(int argc, char *argv[]) {
  if (argc != 2) usage();
  struct sigaction sa;
  sa.sa_flags = 0;
  sa.sa_handler = trata_signals;
  sigfillset(&sa.sa_mask);
  sigaction(SIGHUP, &sa, NULL);

  max = atoi(argv[1]);
  if (mknod("MIPIPE", S_IFIFO | 0666, 0) < 0) {
    if (errno != EEXIST) {
      error_and_exit("Error al crear la pipe\n");
    }
  }
  int pipe_r = open("./MIPIPE", O_RDONLY);
  char c;
  char *buff = malloc(sizeof(char)*100);
  int i = 0;
  int str = 0;
  while (read(pipe_r, &c, sizeof(char)) > 0) {
    if (c == 'd') {
      str = 0;
      if (max > fills_vius) {
        ++fills_vius;
        int ret = fork();
        if (ret < 0) error_and_exit("Error al fork\n");
        if (ret == 0) {
          execlp("./slave", "slave", buff, (char *)NULL);
        }
        else {
          free(buff);
          buff = malloc(sizeof(char)*100);
          i = 0;
        }
      }
      else {
        free(buff);
      }
    }
    if (str) buff[i++] = c;
    if (c == '#') str = 1;
    if (waitpid(-1, NULL, WNOHANG) > 0) {
      --fills_vius;
    }
  }
  free(buff);
  close(pipe_r);
}
