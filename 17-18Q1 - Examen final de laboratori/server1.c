#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

void usage()
{
  char buff[256];
  sprintf(buff, "Usage: server1\n \
  XD lol\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void error_and_exit(char *msg) {
  perror(msg);
  exit(1);
}

int main(int argc, char *argv[]) {
  if (argc != 1) usage();
  if (mknod("MIPIPE", S_IFIFO | 0666, 0) < 0) {
    if (errno != EEXIST) {
      error_and_exit("Error al crear la pipe\n");
    }
  }
  int pipe_r = open("./MIPIPE", O_RDONLY);
  char c;
  char *buff = malloc(sizeof(char)*100);
  int i = 0;
  int str = 0;
  while (read(pipe_r, &c, sizeof(char)) > 0) {
    if (c == '\0') {
      str = 0;
      int ret = fork();
      if (ret < 0) error_and_exit("Error al fork\n");
      if (ret == 0) {
        execlp("./slave", "slave", buff, (char *)NULL);
      }
      else {
        free(buff);
        buff = malloc(sizeof(char)*100);
        i = 0;
      }
    }
    if (str) buff[i++] = c;
    if (c == '#') str = 1;
  }
  waitpid(-1, NULL, 0);
  free(buff);
  close(pipe_r);
}
