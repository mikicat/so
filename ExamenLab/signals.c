#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

int fin_alarma = 0;

//usage function
void usage()
{
  char buff[256];
  sprintf(buff, "Usage: signals N (donde 0 < N <= 10)\n \
  Este programa crea N procesos de forma concurrente.\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void error_and_exit(char *msg, int exit_status)
{
  perror(msg);
  exit(exit_status);
}

void trata_signals(int s) {
  if (s == SIGALRM) {
    fin_alarma = 1;
  }
  else if (s == SIGUSR1) {}
}

int main(int argc,char *argv[]) {
  char buff[256];
  if (argc != 2) usage();

  sigset_t mask;
  sigfillset(&mask);
  sigprocmask(SIG_BLOCK, &mask, NULL);

  struct sigaction sa;
  sa.sa_handler = &trata_signals;
  sa.sa_flags = SA_RESTART;
  sigfillset(&sa.sa_mask);

  sigaction(SIGUSR1, &sa, NULL);
  sigaction(SIGALRM, &sa, NULL);

  int N = atoi(argv[1]);
  int pids[N];

  for (int i = 0; i < N; ++i) {
    int pid = fork();
    if (pid < 0) error_and_exit("Error en el fork.\n", 1);
    else if (pid == 0) {
      alarm(3);

      sigfillset(&mask);
      sigdelset(&mask, SIGINT);
      sigdelset(&mask, SIGUSR1);
      sigdelset(&mask, SIGALRM);
      sigsuspend(&mask);

      if (fin_alarma) {
        sprintf(buff, "%d: Timeout\n", getpid());
        write(1, buff, strlen(buff));
      }

      int secs = alarm(0);
      exit(secs);
    }
    else pids[i] = pid;
  }

  for (int i = 0; i < N; ++i) {
    if (pids[i]%2 == 0) {
      if (kill(pids[i], SIGUSR1) < 0) error_and_exit("Error en el kill.\n", 1);
    }
  }

  int pid_muerto;
  int exitcode;
  while((pid_muerto = waitpid(-1, &exitcode, 0)) > 0) {
    if (pid_muerto%2 == 0) { // Si PID es par
      if (WIFEXITED(exitcode)) { // Si ha muerto por exit
        sprintf(buff, "Hijo %d. Segundos restantes %d\n", pid_muerto, WEXITSTATUS(exitcode));
        write(1, buff, strlen(buff));
      }
    }
  }
}
