Miquel Comas Hervada - Grup 11

 - ¿En qué orden se van a escribir los mensajes?
   ¿Podemos garantizar que será siempre el mismo?
=================================================
El orden en que se van a escribir los mensajes depende de la preferencia que
el sistema operativo decida asignar a los hijos, ya que se crean de forma
concurrente, por lo que no podemos determinar el orden concreto en que se van
a escribir ni garantizar que siempre sea el mismo.
