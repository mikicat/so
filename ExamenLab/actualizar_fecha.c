#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

//usage function
void usage()
{
  char buff[256];
  sprintf(buff, "Usage: actualizar_fecha nombre_fichero1 [nombre_fichero2.. nombre_ficheroN]\n \
  Este programa actualiza las fechas de los ficheros pasados como argumento.\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void error_and_exit(char *msg, int exit_status)
{
  perror(msg);
  exit(exit_status);
}

int main(int argc,char *argv[]) {
  char buff[256];
  if (argc < 2) usage();

  int N = argc-1; // N max = 5
  int pids[N];

  for (int i = 1; i < argc; ++i) {
    int pid = fork();
    if (pid < 0) error_and_exit("Error en el fork.\n", 1);
    else if (pid == 0) {
      execlp("touch", "touch", argv[i], (char *)NULL);
      error_and_exit("Error en execlp.\n", 1); // Nomes si error en execlp
    }
    else pids[i-1] = pid;
  }

  int hijo_muerto;
  while((hijo_muerto = waitpid(-1, NULL, 0)) > 0) {
    for (int i = 0; i < N; ++i) {
      if (pids[i] == hijo_muerto) {
        sprintf(buff, "Fecha %s actualizada por %d\n", argv[i+1], hijo_muerto);
        write(1, buff, strlen(buff));
        break; // Per no fer mes loops al for dels necessaris
      }
    }
  }
}
