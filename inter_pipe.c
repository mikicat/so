#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

void error_and_exit(char* msg) {
  perror(msg);
  exit(1);
}

int main() {
  int pipefds[2];
  int r = pipe(pipefds);
  if (r < 0) error_and_exit("Pipe error");
  for (int i = 0; i < 2; ++i) {
    int ret = fork();
    if (ret < 0) error_and_exit("Fork error");
    if (ret == 0 && i == 0) {
      dup2(pipefds[1], 1);
      close(pipefds[1]); // Cal tancar les pipes abans de mutar
      close(pipefds[0]);
      execlp("ls", "ls", "-a", (char *)NULL);
      error_and_exit("Execlp error");
    }
    else if (ret == 0 && i == 1) {
      dup2(pipefds[0], 0);
      close(pipefds[1]);
      close(pipefds[0]);
      char buff[5000];
      read(0, buff, sizeof(buff));
      write(1, buff, strlen(buff));
    }
  }
  while(waitpid(-1, NULL, 0) > 0);
}
