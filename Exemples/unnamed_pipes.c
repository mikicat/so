#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

//The read and write from pipes without redirecting stdin and stdout are in the named pipes, could be applied in unnamed pipes.

/*
    El canal 0 es stdin
    El canal 1 es stdout
    El canal 2 es stderr

    La terminal només accepta ASCII
    ASCII to integer -> atoi
        int num = atoi(pointer);
        // pointer could be argv[1]
    Integer to ASCII -> sprintf
        int num = 4565;
        sprintf(buff,"The number is %d,number);
*/

void main() {
    int fd[2];
    pipe(fd);
    int pid1 = fork();
    if (pid1 == 0) {
        //redirection stdin, the process reads from the pipe
        //El canal 0 es tanca i es remplaça per fd[0] (pipe read)
        dup2(fd[0],0);
        //always close non used canals
        close(fd[0]); //close pipe read
        close(fd[1]); //close pipe write

        return(0);
        //end the execution of the child
    }
    int pid2 = fork();
    if (pid2 == 0) {
        //redirection stdout, the process writes on the pipe
        //EL canal 1 es tanca i es remplaça per fd[1] (pipe write)
        dup2(fd[1],1);
        //always close non used canals
        close(fd[0]); //close pipe read
        close(fd[1]); //close pipe write

        return(0);
        //end the execution of the child
    }
    close(fd[0]); //close pipe read
    close(fd[1]); //close pipe write
    while (waitpid(-1,NULL,0) > 0);
}