//All the libraries used will be here
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>


// if any global variable is needed
#define variable 'value'

/*
    The process will wait sec seconds and then resume the execution
    int sec = 3;
    sleep(sec);
*/

//usage function
void usage()
{
    printf("Usage: myPS_v0 arg1 [arg2..argn]\n");
    printf("This program executes creates a child process\n");    
}

//function that is called when something goes wrong
//if (something) error_and_exit("Error in whatever", some_number);
void error_and_exit(char *msg, int exit_status)
{
    perror(msg);
    exit(exit_status);
}

//exit_code treatement
void trataExitCode(int pid, int exit_code) 
{
	char buffer[128];
	if (WIFEXITED(exit_code)) {
		int statcode = WEXITSTATUS(exit_code);
		sprintf(buffer, "El proceso %d termian con exit code %d\n", pid, statcode);
	}
	else {
		int signcode = WTERMSIG(exit_code);
		sprintf(buffer, "El proceso %d termina por el signal %d\n", pid, signcode);
	}
	write(1, buffer, strlen(buffer));
}


// example of usage of the exit_code
void main(int argc, char *argv[])
{ 
	int exit_code;
	int pid;
	//...
	while ((pid = waitpid(-1, &exit_code, 0) > 0)) trataExitCode(pid, exit_code);
	exit(0);
}


int main(int argc,char *argv[])
{
    // Usage function different for every program, it writes on the standard output what the program does in case the input was not correct
    if(argc<=1){
        usage();
        return 1;
    }
    
    // Standard output
	int number;
	char character;
    char buff[80];
    sprintf(buff,"The number is %d, the character is %c and the string is %s\n",number,character/*,string*/);
    write(1,buff,strlen(buff));
    
}
