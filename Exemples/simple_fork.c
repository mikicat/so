#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int main(int argc,char *argv[])
{
    int pid;
    char buffer[80];

    sprintf(buffer,"Antes del fork: Soy el proceso %d\n",getpid());
    write (1, buffer, strlen(buffer));
    
    pid=fork();
    
    // The parent will have to wait until the child has finished his execution
    waitpid(-1,NULL,0);

    //The switch can also be done with a simple if
    switch (pid) { /* Esta linea la ejecutan tanto el padre como el hijo */
        case 0: /* Escribe aqui el codigo del proceso hijo */ 
            sprintf(buffer,"HIJO: Soy el proceso %d\n",getpid());
            write(1, buffer, strlen(buffer));
            /* Termina su ejecución */
            exit(0);

        
        case -1: /* Se ha producido un error */
            sprintf(buffer,"Se ha prodcido une error\n");
            write(1, buffer, strlen(buffer));
            break;
        // Always check this case because if not the program won't work
        /* Another way to check it
            
            if (pid<0) error_and_exit("Error in fork",1);
            
        */

        default: /* (pid !=0) && (pid !=-1) */
            /* Escribe aqui el codigo del padre */
            sprintf(buffer,"PADRE: Soy el proceso %d\n",getpid());
            write(1, buffer, strlen(buffer));
    }
    
    sprintf(buffer,"Solo lo ejecuta el padre: Soy el proceso %d\n",getpid());
    write(1, buffer, strlen(buffer));
    
    return 0;
}
