#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>


//The redirection of stdin and stdout are in the unnamed pipes, but could be applied in named pipes.

//The named pipe should be created before using $ mknod pipe p, where pipe could any other name
//The pipe should be in the same directory, if not, write the absolute path in the open.

//Creation of the pipe using the konsole
//$ mknod pipe p

//Cration of the pipe in the program.
/*
    if (mknod("my_pipe", S_IFIFO|0666, 0) < 0) {
        if (errno != EEXIST) {
            perror ("Error creating named pipe\n");
            exit (1);
        }
    }
    */

//Lector de la pipe
int main() {
    int fd = open("./pipe",O_RDONLY);
    if (fd < 0) error_and_exit("Error opening the pipe", 1);

    //read byte by byte
    char c;
    int n;
    while ((n = read (0, &c, 1)) > 0) write(1,&c,1);

    //read normaly
    char buffer[256];
    int ret;
    while ((ret = read(fd,&buffer,sizeof(buffer))) > 0) write(1,buffer,ret);
}

//Escriptor a la pipe
int main() {
    char buffer[256];
    int fd = open("./pipe",O_WRONLY|O_NONBLOCK); //O_NONBLOCK optional
    if (fd < 0) error_and_exit("Error opening the file", 1);
    int ret;
    //reads from the stdin
    while ((ret = read(0,&buffer,ret)) > 0) write(fd,buffer,ret);
}