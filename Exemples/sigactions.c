#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>

void error_y_exit(char *msg,int exit_status)
{
    perror(msg);
    exit(exit_status);
}

void my_function(int s) {
    // s is the signal that has been recived (represented by its number,
    // for example SIGUSR1 = 10 and SIGUSR2 = 12)
    // https://www.bogotobogo.com/Linux/linux_process_and_signals.php list of int signals
    char buffer[64];
    sprintf(buffer, "%d", s);
    write(1,buffer,strlen(buffer));
    //whatever it is supposed to do
}

int main (int argc,char * argv[]) {
    
    //Creation of the mask and the struct
    struct sigaction sa;
    sigset_t mask;
    
    //Modification of the default action of that signal
    sa.sa_handler = &my_function;
    sa.sa_flags = SA_RESTART;
    sigfillset(&sa.sa_mask);
    if (sigaction(SIGUSR1, &sa, NULL) < 0 || sigaction(SIGUSR2, &sa, NULL) < 0) error_y_exit("sigaction", 1);

    //Blocking the signals before entering the sigsuspend
    sigemptyset(&mask);
    sigaddset(&mask, SIGUSR1);
    sigaddset(&mask, SIGUSR2);
    sigprocmask(SIG_BLOCK,&mask, NULL);
    
    //Configuring the sigsuspend
    sigfillset(&mask);
    sigdelset(&mask, SIGINT);
    sigdelset(&mask, SIGUSR1);
    sigdelset(&mask, SIGUSR2);
    sigsuspend(&mask);
    
    exit(1);
}
