#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>


void error_and_exit(char* msg, int stat) {
    perror(msg);
    exit(stat);
}

//reads and writes form a file
int main() {
    char buffer[128];
    int fd = open("./file", O_RDWR);
    if (fd < 0) error_and_exit("Error opening the file", 1);
    int ret1 = read(fd, buffer, strlen(buffer)); //reads from the file
    int ret2 = write(fd, buffer, ret1); //wrties on that same file
}
