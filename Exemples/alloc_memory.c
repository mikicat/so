#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>

//function that stores in a vector located in the heap the pids of its children
//using the system call sbrk
void main(int argc, char *argv[]) { 
    int num_procs = atoi(argv[1]); 
    int *pids; 
    pids = sbrk(num_procs*sizeof(int)); 
    for(int i = 0; i < 10; i++) { 
        pids[i] = fork(); 
        /*if (pids[i]==0) { …. } */
    }
    sbrk(-1*num_procs*sizeof(int));
}

//function that stores in a vector located in the heap the pids of its children
//using the C libary functions
void main(int argc, char *argv[]) { 
    int num_procs = atoi(argv[1]); 
    int *pids; 
    pids = malloc(num_procs*sizeof(int)); 
    for(int i = 0; i < 10; i++) { 
        pids[i] = fork();
        /*if (pids[i]==0) { …. } */
    }
    free(pids);
}