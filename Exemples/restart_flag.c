#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>

void my_function(int s) {
    char buff[128] = "signal recived\n";
    write(1,&buff,strlen(buff));
}

// with the flag the program resumes de I/O operation after the signal is handeled
void main(){
    char c;

    struct sigaction sa;
    
    sa.sa_handler = &my_function;
    //sa.sa_flags = SA_RESTART;     without this flag the management is equal to the UNIX management
    sigfillset(&sa.sa_mask);
    sigaction(SIGINT, &sa, NULL);
    
    int ret = read(0, &c, sizeof(char));
    char buffer[128];
    
    if (ret < 0) {
        if (errno == EINTR) {
            sprintf(buffer, "Signal interrupted read\n");
            write(1,buffer,strlen(buffer));
        }
        else {
            sprintf(buffer, "Read with error\n");
            write(1,buffer,strlen(buffer));
        }
    }
    else {
        sprintf(buffer, "Correct read\n");
        write(1,buffer,strlen(buffer));
    }
    
    //write(1, &c, sizeof(char));

}
