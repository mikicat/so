#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

void error_and_exit(char* msg, int stat) {
    perror(msg);
    exit(stat);
}

//writes in a file that contains ABCD, ABXCD
int main() {
    char buffer[128];
    int fd = open("salida.txt", O_RDWR);
    if (fd < 0) error_and_exit("Error opening the file", 1);
    lseek(fd, -2, SEEK_END);
    int ret = read(fd, buffer, 2*sizeof(char)); //reads the two last chars
    lseek(fd, -2, SEEK_END); //after the read the position is at the end again, so this is necessary
    write(fd, "X", sizeof(char)); //everything after the X is erased
    write(fd, buffer, ret); //writes what was after the x
}

//does the same that before but in any position
int main (int argc, char * argv[]) {
    char buffer[128];
    int fd = open("file", O_RDWR);
    if (fd < 0) error_and_exit("Error opening the file", 1);
    int pos = atoi(argv[1]);
    pos = pos * -1;
    lseek(fd, pos, SEEK_END);
    int ret = read(fd, buffer, -1*pos*sizeof(char));
    lseek(fd, pos, SEEK_END);
    write(fd, "X", sizeof(char));
    write(fd, buffer, ret);
}


//invierte el contenido de un fitxero
int main (int argc,char * argv[]) {
    char c;
    int fd1 = open("file", O_RDWR);
    if (fd1 < 0) error_and_exit("Error opening the file", 1);
    int fd2 = creat("file.inv", O_CREAT | 0600);
    if (fd2 < 0) error_and_exit("Error creating the file", 1);
    int pos = -1;
    lseek(fd1, pos, SEEK_END);
    int p = 1;
    while (read(fd1, &c, sizeof(char)) > 0 && p >= 0) {
        write(fd2, &c, sizeof(char));
        --pos;
        p = lseek(fd1,pos,SEEK_END);
    }
}

//creates a file
int main() {
    char buffer[256];
    // O_CREAT -> if pathname does not exists, create it as a regular file
    int fd = creat("salida.txt", O_CREAT | 0600);
    if (fd < 0) error_and_exit("Error creating the file", 1);
    sprintf(buffer,"ABCD");
    write(fd,buffer,strlen(buffer));
}
