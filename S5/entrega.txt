Miquel Comas Hervada - Grup 11

PREGUNTA 35.¿Qué  variables  aparecen  en  la  salida  del  nm de  mem1_dynamic?
¿Qué dirección ocupa cada una? Indica a que región pertenece cada variable según
la salida del nm y el tipo de variable (local o global).
================================================================================
Sólo aparece la variable globalArray ya que es una variable global, en la dirección
0000000000004080, y al no estar inicializada está en la sección .bss.

PREGUNTA 36.Para los dos ejecutables, compilado estático y dinámico, observa su
tamaño, la salida de los comandos nm y objdump ¿En qué se diferencian los dos
ejecutables?
================================================================================
El tamaño de el static es de 755888 bytes y el de el dynamic es de 16896 bytes.
Además, en el static se incluyen las librerías de sistema por lo que la salida
de nm y objdump contiene mucha más información (porque hay muchos más objetos).

PREGUNTA 37.Observa el contenido del fichero maps del /proc para cada proceso y
apunta para cada región la dirección inicial, dirección final y etiqueta asociada.
¿Qué diferencia hay entre el maps de cada proceso?
================================================================================
Contenido del maps del fichero dinámico (a partir del heap):
56089d95b000-56089d97c000 [heap]
7fdb4e40d000-7fdb4e42f000 /usr/lib/x86_64-linux-gnu/libc-2.28.so
7fdb4e42f000-7fdb4e577000 /usr/lib/x86_64-linux-gnu/libc-2.28.so
7fdb4e577000-7fdb4e5c3000 /usr/lib/x86_64-linux-gnu/libc-2.28.so
7fdb4e5c3000-7fdb4e5c4000 /usr/lib/x86_64-linux-gnu/libc-2.28.so
7fdb4e5c4000-7fdb4e5c8000 /usr/lib/x86_64-linux-gnu/libc-2.28.so
7fdb4e5c8000-7fdb4e5ca000 /usr/lib/x86_64-linux-gnu/libc-2.28.so

7fdb4e5f0000-7fdb4e5f1000 /usr/lib/x86_64-linux-gnu/ld-2.28.so
7fdb4e5f1000-7fdb4e60f000 /usr/lib/x86_64-linux-gnu/ld-2.28.so
7fdb4e60f000-7fdb4e617000 /usr/lib/x86_64-linux-gnu/ld-2.28.so
7fdb4e617000-7fdb4e618000 /usr/lib/x86_64-linux-gnu/ld-2.28.so
7fdb4e618000-7fdb4e619000 /usr/lib/x86_64-linux-gnu/ld-2.28.so

7ffdf0e29000-7ffdf0e4b000 [stack]
7ffdf0e64000-7ffdf0e67000 [vvar]
7ffdf0e67000-7ffdf0e69000 [vdso]

Contenido del maps del fichero estático (a partir del heap):
00f97000-00fba000 [heap]
7fffd524d000-7fffd526f000 [stack]
7fffd535a000-7fffd535d000 [vvar]
7fffd535d000-7fffd535f000 [vdso]

En el fichero maps del /proc del dinámico hay referencias a las librerías del sistema,
en concreto la librería /usr/lib/x86_64-linux-gnu/ld-2.28.so; en cambio el fichero
maps del estático no tiene ninguna de esas referencias ya que incluye las librerías
dentro del propio ejecutable.

PREGUNTA 38.¿A qué región de las descritas en el maps pertenece cada variable y
cada zona reservada con malloc?
Apunta la dirección inicial, dirección final y el nombre de la región.
===============================================================================
Pertenecen a la región del heap.
En el dinámico: 56089d95b000-56089d97c000 [heap]
En el estático: 00f97000-00fba000 [heap]

PREGUNTA 39.Para cada ejecución apunta el número de mallocs hechos y la dirección
 inicial y final del heap que muestra el fichero maps del /proc.
¿Cambia el tamaño según el parámetro de entrada? ¿Por qué?
===============================================================================
3 iteraciones: heap 555920122000-555920143000 -> tamaño = 0x21000
5 iteraciones: heap 5634005a8000-5634005c9000-> tamaño = 0x21000
100 iteraciones: heap 55b316f3a000-55b316fbe000 -> tamaño = 0x84000
Entre 3 y 5 iteraciones el tamaño no varia, para 100 sí. Esto es porque malloc
comprueba si hay suficiente memoria disponible (el caso de 3 y 5), y si no hay
llama a sbrk() para reservar más memoria del heap.

PREGUNTA 40.¿Cuál es el tamaño del heap en este caso? Explica los resultados.
===============================================================================
3 iteraciones: heap 5653b8527000-5653b8548000 -> tamaño = 0x21000
5 iteraciones: heap 55e4e8ef2000-55e4e8f13000 -> tamaño = 0x21000
100 iteraciones: heap 558249feb000-55824a00c000 -> tamaño = 0x21000
El tamaño del heap es constante porque el espacio que se reserva con malloc se
libera antes de volver a llamar a malloc, por lo que siempre se está reservando
y liberando una zona del mismo tamaño, así que el tamaño final no varía.

PREGUNTA 41.Para cada ejecución apunta el número de mallocs hechos y la dirección
inicial y final del heap que se muestra en el maps del /proc.
¿Cambia el tamaño del heap respecto al observado en la pregunta 6? ¿Por qué?
===============================================================================
3 sbrks: heap 55a091de1000-55a091de4000 -> tamaño = 0x3000
5 sbrks: heap 55d256caa000-55d256caf000 -> tamaño = 0x5000
100 sbrks: heap 55f98304e000-55f9830b2000 -> tamaño = 0x64000
Sí, cambia el tamaño del heap, porque sbrk() siempre reserva nueva memoria en el
heap, en cambio, malloc comprueba si hay suficiente, y solo en caso de que no haya
reserva nueva memoria.


PREGUNTA 42.¿Qué error contiene el código del programa? ¿Por qué el programa no
falla en las primeras iteraciones? Propón una alternativa a este código que evite
que se genere la excepción, detectando, antes de que pase,que vamos a hacer un
acceso fuera del espacio de direcciones.
===============================================================================
El error es que se intenta acceder a una dirección de memoria fuera de la reservada
por malloc. El programa no falla en las primeras iteraciones porque el puntero
va augmentando de 1 en 1, por lo que hasta que no hace sizeof(int) iteraciones
no provoca el error.
Lo solucionaría con sbrk(0), comprobando qué dirección es el final del heap.
