//All the libraries used will be here
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

// if any global variable is needed
#define variable 'value'

//usage function
void usage()
{
    printf("Usage: myPS_v0 arg1 [arg2..argn]\n");
    printf("This program executes creates a child process\n");    
}

//function that is called when something goes wrong
//if (something) error_and_exit("Error in whatever", some_number);
void error_and_exit(char *msg, int exit_status)
{
    perror(msg);
    exit(exit_status);
}

int main(int argc,char *argv[])
{
    // Usage function different for every program, it writes on the standard output what the program does in case the input was not correct
    if(argc<=1){
        usage();
        return 0;
    }
    
    // Standard output
    char buff[80];
    sprintf(buff,"The number is %d, the character is %c and the string is %s\n",number,character,string);
    write(1,buff,strlen(buff));
    
}
