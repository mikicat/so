#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void error_and_exit(char *msg, int exit_status)
{
    perror(msg);
    exit(exit_status);
}

int main(int argc,char *argv[])
{
    //Concurrent execution of the child processes
    for (int i=0; i<argv[1]; ++i) {
        int pid;
        char buffer[80];
        
        pid=fork();
        if (pid<0) error_and_exit("Error in fork",1);
        
        if (pid==0) {
            sprintf(buffer,"HIJO: soy el proceso %d\n",getpid());
            write(1, buffer, strlen(buffer));
            //whatever is the function of the child
        }
        else {
            sprintf(buffer,"PADRE: soy el proceso %d\n",getpid());
            write(1, buffer, strlen(buffer));
        }
    }
    while(waitpid(-1,NULL,0)>0);
    
    //Sequential execution of the child processes
    for (int i=0; i<argv[1]; ++i) {
        int pid;
        char buffer[80];
        
        pid=fork();
        if (pid<0) error_and_exit("Error in fork",1);
        
        if (pid==0) {
            sprintf(buffer,"HIJO: soy el proceso %d\n",getpid());
            write(1, buffer, strlen(buffer));
            //whatever is the function of the child
        }
        else {
            sprintf(buffer,"PADRE: soy el proceso %d\n",getpid());
            write(1, buffer, strlen(buffer));
        }
        waitpid(-1,NULL,0);
    }
    
    //Store the pid of the childs (concurrent execution)
    int pid[N];
    for (hijos=0;hijos<N;hijos++) {
        sprintf(buff, "Creando el hijo numero %d\n", hijos);
        write(1, buff, strlen(buff)); 
            
        pid[hijos]=fork();
        if (pid[hijos]==0) {
            //whatever is the function of the child
        } 
        else if (pid[hijos]<0) error_y_exit("Error en el fork", 1);
    }
    for (char i=0; i<10; i++) {
        kill(pid[i],SIGKILL);
    }
}
