#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

void error_y_exit(char *msg,int exit_status)
{
    perror(msg);
    exit(exit_status);
}

int main(int argc,char *argv[])
{
    //Creation of the mask
    sigset_t mask;
    
    //Simple block of a signal, the program keeps running. It can also be used to not take into consideration a signal before a sigsuspend
    sigemptyset(&mask);
    sigaddset(&mask,SIGNAL_NAME);
    sigprocmask(SIG_BLOCK,&mask, NULL);
    
    
    //The program waits until it recieves one of the alarms that are not blocked
    //Very usual to put "alarm(n);" before this code. Then SIGNAL_NAME would be SIGALRM
    sigfillset(&mask);
    sigdelset(&mask, SIGNAL_NAME);
    sigdelset(&mask, SIGINT); //For the Ctrl+c interruption 
    sigsuspend(&mask);
    
    //Sending signals between processes
    if (kill (pid, SIGNAL_NAME) < 0) error_y_exit("kill", 1);

}

