#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
  int pipefd[2];
  int p = pipe(pipefd);
  int ret = fork();
  if (ret < 0) exit(1);
  else if (ret == 0) {
    dup2(pipefd[0], 0);
    close(pipefd[0]);
    close(pipefd[1]);
    execlp("cat", "cat", (char *)NULL);
  }
  else {
    close(pipefd[0]);
    char buff[7];
    sprintf(buff, "Inicio\n");
    write(pipefd[1], buff, strlen(buff));
    close(pipefd[1]);
    waitpid(-1, NULL, 0);
    sprintf(buff, "Fin\n");
    write(1, buff, strlen(buff));
  }
}
