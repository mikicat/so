Previo S8 - Miquel Comas Hervada
Comandos para redireccionar cat a pipe y leer de la pipe:
=======================================
$ mknod mypipe p
$ cat > mypipe
(y desde otra shell: )
$ cat < mypipe


¿Es posible comunicar los dos comandos ‘cat’ desde dos terminales diferentes a
través de una pipe sin nombre (por ejemplo, utilizando un pipeline de la shell
visto en la sesión anterior)? ¿y desde el mismo terminal? Razona la respuesta
en el fichero “previo.txt”.
 =======================================
 No se puede crear una pipe sin nombre en dos shells distintas porque las pipes
 sin nombre están ligadas a un proceso.
 Desde la misma shell sí que se puede hacer ya que es el mismo proceso, que
 crea una pipe y muta a otro proceso.


Escribe en el fichero “previo.txt” el fragmento de código que deberíamos
añadir a un programa cualquiera para redireccionar su entrada estándar al
extremo de escritura de una pipe sin nombre utilizando las llamadas al sistema
close y dup. Imagina que el descriptor de fichero asociado al extremo de
escritura de la pipe es el 4.
=======================================
dup2(0, 4);
close(4);


El fichero “socketMng.c” contiene unas funciones de gestión básica de sockets
(creación, solicitud de conexión, aceptación de conexión y cierre de
dispositivos virtuales). Analiza en detalle el código de la función createSocket
y serverConnection, y busca en el man el significado de las llamadas a sistema
socket, bind, listen y accept. Explica en el fichero “previo.txt” paso a paso
lo que hacen estas dos funciones
=======================================
createSocket(): se crea una dirección con strcpy() y con la llamada a sistema
socket() se crea un socket nuevo (si no hay ningún error).
Usando la llamada a sistema bind() se le asigna la dirección del socket que se ha
creado previamente.
La función listen() deja el socket en espera pasiva hasta que llega una transmisión.
serverConnection() comprueba que no haya habido ningún error en el proceso de
creación del socket; si no ha fallado devolverá el descriptor de fichero. (Y si falla devuelve -1)
