#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

int main(int argc, char *argv[]){
	char c;
	int fd = open("./mypipe",O_WRONLY|O_NONBLOCK);
	if ((fd < 0) && (errno == ENXIO)) perror("Esperant a un lector");
	fd = open("./mypipe",O_WRONLY);
	int ret;
	while((ret = read(fd, &c, 1)) > 0){
		write(0, &c, 1);
	}
}
