#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char *argv[]){
	char c;
	int fd = open("./mypipe",O_RDONLY);
	int ret;
	while((ret = read(fd, &c, 1)) > 0) write(0, &c, 1);
  close(fd);
}
