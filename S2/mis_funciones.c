#include "mis_funciones.h"
unsigned int char2int(char c) {
  return c - '0';
}

int mi_atoi(char *s) {
  int n = 0;
  int i = strlen(s) - 1;
  int f;

  int exp = 1;
  if (s[0] == '-') f = 1;
  else f = 0;
  while (i >= f) {
    n += char2int(s[i]) * exp;
    exp *= 10;
    --i;
  }
  return n;
}

int esNumero(char *str) {
  char buff[128];

  if (str == NULL) return 0;

  int i = 0;
  while (str[i] != '\0' && i < MAX_SIZE) {
    if (*(str+i) < '0' || *(str+i) > '9') {
      if (i != 0 || (i == 0 && *(str+i) != '-')) return 0;
    }
    ++i;
  }
  return 1;
}

void Usage() {
  char buff[256];
  sprintf(buff, "Usage:suma arg1 arg2 [arg3..argn]\n");
  write(1, buff, strlen(buff));
  sprintf(buff, "Este programa calcula la suma de los argumentos que recibe\n");
  write(1, buff, strlen(buff));
}
