#include "mis_funciones.h"




int main(int argc, char *argv[]) {
  char buffer[128];
  int suma = 0;
  if (argc < 3) {
    Usage();
    return 0;
  }
  for (int i = 1; i < argc; ++i) {
    if (esNumero(argv[i]) == 1) {
      suma += mi_atoi(argv[i]);
    }
    else {
      sprintf(buffer, "Error: el parámetro \"%s\" no es un número\n", argv[i]);
      write(1, buffer, strlen(buffer));
      return 0;
    }
  }
  sprintf(buffer, "La suma es %d\n", suma);
  write(1, buffer, strlen(buffer));

  return 0;
}
