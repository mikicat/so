#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
  char buff[12];
  int c = 0;
  if (argc > 1) {
    ++c;
    for (int i = 0; *(argv[1] + i) != '\0'; ++i) {
      if (*(argv[1] + i) == ' ' || *(argv[1] + i) == '\n') {
        ++c;
      }
    }
  }
  sprintf(buff, "%d palabras\n", c);
  write(1, buff, strlen(buff));
  return 0;
}
