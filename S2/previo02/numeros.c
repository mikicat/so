#include <stdio.h>
#include <unistd.h>
#include <string.h>
#define MAX_SIZE 8

int esNumero(char *str) {
  char buff[128];

  if (str == NULL) return 0;

  int i = 0;
  while (str[i] != '\0' && i < MAX_SIZE) {
    if (*(str+i) < '0' || *(str+i) > '9') {
      if (i != 0 || (i == 0 && *(str+i) != '-')) return 0;
    }
    ++i;
  }
  return 1;
}

int main(int argc, char *argv[]) {
  char buffer[128];
  for (int i = 1; i < argc; ++i) {
    if (esNumero(argv[i]) == 1) {
      sprintf(buffer, "%s es un numero\n", argv[i]);
    }
    else {
      sprintf(buffer, "%s no es un numero\n", argv[i]);
    }
    write(1, buffer, strlen(buffer));
  }

  return 0;
}
