PREGUNTA 17.¿Qué opción has tenido que añadir al gcc para generar el fichero objecto?
¿Qué opción  has  tenido  que  añadir  al  gcc  para  que  el  compilador  encuentre
el  fichero mis_funciones.h?

Para generar el fichero objeto he añadido la opción '-c' al gcc.
Para que el compilador encuentre mis_funciones.h se puede añadir
la opción -I./ o simplemente con `gcc -c suma.c` ya se genera el
fichero objeto de suma, ya que el propio compilador encuentra el
fichero `mis_funciones.h`.
