#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <errno.h>

void trata_signal(int s) {
  if (s == SIGINT) {
    char buff[256];
    sprintf(buff, "SIGINT recibido.\n");
    write(1, buff, strlen(buff));
  }
}

main(){
  char buff[256];
  struct sigaction sa;
  sa.sa_handler = &trata_signal;
  sa.sa_flags = 0;
  sigfillset(&sa.sa_mask);
  sigaction(SIGINT, &sa, NULL);
	char c;


	int ret = read(0, &c, sizeof(char));
  if (ret < 0) {
    int err = errno;
    if (err == EINTR) {
      sprintf(buff, "Read interrumpido por signal.\n");
    }
    else {
      sprintf(buff, "Read con error.\n");
    }
    write(1, buff, strlen(buff));
  }
  else {
    sprintf(buff, "Read correcto.\n");
    write(1, buff, strlen(buff));
  }
	write(1, &c, sizeof(char));

}
