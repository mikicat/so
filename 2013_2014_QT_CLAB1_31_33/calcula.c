#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

#define SYSCALL_ERROR 1
int ITERACION_ACTUAL = -1;
// Esta funcion no es relevante, solo pierde tiempo
void calcula(int i)
{
	int it;
	int fd;
	for (it=0;it<i;it++){
		fd=open("F",O_WRONLY|O_CREAT|O_TRUNC,S_IRWXU);
		if (fd<0){
			perror("Error en el open");
			exit(SYSCALL_ERROR);
		}
		write(fd,&it,sizeof(it));
		close(fd);
	}
}

void trata_signal(int s) {
	char buff[256];
	if (s == SIGUSR1 || s == SIGALRM) {
		if (ITERACION_ACTUAL < 0) sprintf(buff, "Aun no ha entrado en el bucle.\n");
		else sprintf(buff, "Iteracion actual: %d\n", ITERACION_ACTUAL);
		write(1, buff, strlen(buff));
		if (s == SIGALRM) exit(1);
	}
}

void usage() {
  char buff[256];
  sprintf(buff, "usage: calcula ITERACIONES_MAIN INTERACIONES_CALCULA MAX_SECS\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void main(int argc,char *argv[])
{
	if (argc != 4) usage();

	struct sigaction sa;
	sa.sa_handler = &trata_signal;
	sa.sa_flags = SA_RESTART;
  sigfillset(&sa.sa_mask);
	sigaction(SIGUSR1, &sa, NULL);
	sigaction(SIGALRM, &sa, NULL);

	int i,ITERACIONES_MAIN=atoi(argv[1]),ITERACIONES_CALCULA=atoi(argv[2]);
	char b[128];
	alarm(atoi(argv[3]));
	for (i=0;i<ITERACIONES_MAIN;i++){
		ITERACION_ACTUAL=i;
		calcula(ITERACIONES_CALCULA);
	}
}
