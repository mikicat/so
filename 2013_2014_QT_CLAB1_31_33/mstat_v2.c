#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>

void error_y_exit(char *msg,int exit_status)
{
    perror(msg);
    exit(exit_status);
}

void trat_exit(int exitcode) {
  char buff[256];
  if (WIFEXITED(exitcode)) {
    if (WEXITSTATUS(exitcode) == 0) {
      sprintf(buff, "stat ejecutado correctamente.\n");
    }
    else {
      sprintf(buff, "stat ha detectado un problema con el fichero.\n");
    }
    write(1, buff, strlen(buff));
  }
}

void usage() {
  char buff[256];
  sprintf(buff, "usage: mstat nom_fitxer1 [nom_fitxer.. nom_fitxerN]\n");
  write(1, buff, strlen(buff));
  exit(0);
}

int main(int argc, char *argv[]) {
  int exitcode;
  if (argc < 2) usage();
  for (int i = 1; i < argc; ++i) {
    char buff[256];
    int pid = fork();
    if (pid < 0) error_y_exit("Error en fork()\n", 1);
    else if (pid == 0) {
      sprintf(buff, "Ejecutamos el comando stat del fichero %s\n", argv[i]);
      write(1, buff, strlen(buff));
      //execlp("stat", "stat", argv[i], (char *)NULL);
      exit(0);
    }
    waitpid(pid, &exitcode, 0);
    trat_exit(exitcode);
  }
}
