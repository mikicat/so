#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>

int PID;

void error_y_exit(char *msg,int exit_status)
{
    perror(msg);
    exit(exit_status);
}

void usage() {
  char buff[256];
  sprintf(buff, "usage: signal PID\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void trata_alarma(int s) {
  int send = kill(PID, SIGUSR1);
  if (send < 0) error_y_exit("Error al enviar SIGUSR1, el proceso no existe", 1);
}

int main(int argc, char *argv[]) {
  if (argc != 2) usage();
  PID = atoi(argv[1]);
  sigset_t mask;
  sigemptyset(&mask);
  sigaddset(&mask, SIGALRM);
  sigprocmask(SIG_BLOCK, &mask, NULL);


  struct sigaction sa;
  sa.sa_handler = &trata_alarma;
  sa.sa_flags = SA_RESTART;
  sigfillset(&sa.sa_mask);
  sigaction(SIGALRM, &sa, NULL);


  sigfillset(&mask);
  sigdelset(&mask, SIGINT);
  sigdelset(&mask, SIGALRM);
  alarm(1);
  sigsuspend(&mask);
}
