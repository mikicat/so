#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>

void error_y_exit(char *msg,int exit_status)
{
    perror(msg);
    exit(exit_status);
}

void usage() {
  char buff[256];
  sprintf(buff, "usage: hermano2 N(num hijos)\n");
  write(1, buff, strlen(buff));
  exit(0);
}

int main(int argc, char *argv[]) {
  char buff[256];
  if (argc != 2) usage();
  int pids[atoi(argv[1])];

  for (int i = 0; i < atoi(argv[1]); ++i) {
    int pid = fork();
    if (pid < 0) error_y_exit("Error en el fork.\n", 1);
    else if (pid == 0) {
      int oldpid;
      if (i == 0) oldpid = getppid();
      else oldpid = pids[i-1];
      sprintf(buff, "PID anterior: %d\n", oldpid);
      write(1, buff, strlen(buff));
      sprintf(buff, "%d", oldpid);
      if (execlp("./signal", "signal", buff, (char *)NULL) < 0) error_y_exit("Error en el execlp.\n", 1);
      exit(0);
    }
    else pids[i] = pid;
  }

  sigset_t mask;
  sigfillset(&mask);
  sigprocmask(SIG_BLOCK, &mask, NULL);
  int pid_muerto;
  int exitcode;

  while ((pid_muerto = waitpid(-1, &exitcode, 0)) > 0) {
    if (WIFEXITED(exitcode)) {
      sprintf(buff, "PID %d: Muerte por exit\n", pid_muerto);
      write(1, buff, strlen(buff));
    }
    else if (WTERMSIG(exitcode)) {
      sprintf(buff, "PID %d: Muerte por signal\n", pid_muerto);
      write(1, buff, strlen(buff));
    }
  };
}
