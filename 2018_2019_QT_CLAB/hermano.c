#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>

void error_y_exit(char *msg,int exit_status)
{
    perror(msg);
    exit(exit_status);
}

void usage() {
  char buff[256];
  sprintf(buff, "usage: hermano N(num hijos)\n");
  write(1, buff, strlen(buff));
  exit(0);
}

int main(int argc, char *argv[]) {
  char buff[256];
  if (argc != 2) usage();
  int pids[atoi(argv[1])];
  for (int i = 0; i < atoi(argv[1]); ++i) {
    int pid = fork();
    if (pid < 0) error_y_exit("Error en el fork.\n", 1);
    else if (pid == 0) {
      if (i == 0) sprintf(buff, "PID anterior: %d\n", getppid());
      else sprintf(buff, "PID anterior: %d\n", pids[i-1]);
      write(1, buff, strlen(buff));
      exit(0);
    }
    else pids[i] = pid;
  }
  sigset_t mask;
  sigfillset(&mask);
  sigprocmask(SIG_BLOCK, &mask, NULL);
  while (waitpid(-1, NULL, 0) > 0);
}
