#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

void usage()
{
  char buf[256];
  sprintf (buf, "Usage: spawnA N\n \
  Este programa crea N hijos que ejecutan hijo1\n");
  write(1, buf, strlen (buf));
  exit(0);
}

void error_and_exit(char *msg) {
  perror(msg);
  exit(1);
}

int main(int argc, char *argv[]) {
  if (argc != 2) usage();
  int n = atoi(argv[1]);
  int i = 0;
  for (i; i < n; ++i) {
    int ret = fork();
    if (ret < 0) error_and_exit("Error en el fork\n");
    else if (ret == 0) {
      char buff[100];
      sprintf(buff, "%d", i);
      execlp("./hijo1", "hijo1", buff, (char *)NULL);
      error_and_exit("Error al mutar a hijo1\n");
    }
  }
  while(waitpid(-1, NULL, 0) > 0) {
    int ret = fork();
    if (ret < 0) error_and_exit("Error en el fork\n");
    if (ret == 0) {
      char buff[100];
      sprintf(buff, "%d", i);
      execlp("./hijo1", "hijo1", buff, (char *)NULL);
      error_and_exit("Error al mutar a hijo1\n");
    }
    else ++i;
  }
}
