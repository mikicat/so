#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

int *pids;
int n;

void usage()
{
  char buf[256];
  sprintf (buf, "Usage: spawnC N\n \
  Este programa crea N hijos que ejecutan hijo1\n");
  write(1, buf, strlen (buf));
  exit(0);
}

void error_and_exit(char *msg) {
  perror(msg);
  exit(1);
}

void trata_signals(int s) {
  if (s == SIGUSR1) {
    char buff[20];
    sprintf(buff, "PIDs de los hijos vivos:\n");
    write(1, buff, strlen(buff));
    for (int i = 0; i < n; ++i) {
      sprintf(buff, "%d\n", pids[i]);
      write(1, buff, strlen(buff));
    }
  }
}

int find_index(int pid_muerto) {
  for (int i = 0; i < n; ++i) {
    if (pids[i] == pid_muerto) return i;
  }
  return -1;
}

int main(int argc, char *argv[]) {
  if (argc != 2) usage();

  // Bloqueamos todas las señales preventivamente
  sigset_t mask;
  sigfillset(&mask);
  sigdelset(&mask, SIGINT); // Dejamos el SIGINT libre por buena praxis
  sigprocmask(SIG_BLOCK, &mask, NULL);
  // Tratamiento para el SIGUSR1
  struct sigaction sa;
  sa.sa_handler = &trata_signals;
  sa.sa_flags = SA_RESTART;
  sigfillset(&sa.sa_mask);
  sigaction(SIGUSR1, &sa, NULL);

  n = atoi(argv[1]);
  pids = malloc(sizeof(int)*n);
  int i = 0;
  for (i; i < n; ++i) {
    int ret = fork();
    if (ret < 0) error_and_exit("Error en el fork\n");
    else if (ret == 0) {
      char buff[100];
      sprintf(buff, "%d", i);
      execlp("./hijo1", "hijo1", buff, (char *)NULL);
      error_and_exit("Error al mutar a hijo1\n");
    }
    else {
      pids[i] = ret;
    }
  }

  // Desbloqueamos SIGUSR1 para que nos puedan llegar
  sigemptyset(&mask);
  sigaddset(&mask, SIGUSR1);
  sigprocmask(SIG_UNBLOCK, &mask, NULL);

  int pid_muerto;
  while((pid_muerto = waitpid(-1, NULL, 0)) > 0) {
    int ret = fork();
    if (ret < 0) error_and_exit("Error en el fork\n");
    if (ret == 0) {
      char buff[100];
      sprintf(buff, "%d", i);
      execlp("./hijo1", "hijo1", buff, (char *)NULL);
      error_and_exit("Error al mutar a hijo1\n");
    }
    else {
      ++i;
      int index = find_index(pid_muerto);
      pids[index] = ret;
    }
  }
  free(pids);
}
