#include <error.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>

void
Usage ()
{
  error (1, 0, "Invalid argument.\nCorrect usage: hijo2 ident");
}

void error_and_exit(char *msg) {
  perror(msg);
  exit(1);
}

int
main (int argc, char *argv[])
{
  int id, minutes = 0;
  char s[80];

  if (argc != 2)
    Usage ();

  id = atoi (argv[1]);

  sprintf (s, "Hijo2[id=%02d pid=%5d] created\n", id, getpid ());
  if (write (2, s, strlen (s)) < 0)
    error (1, errno, "write");

  int fd = open("./input.txt", O_RDONLY);
  if (fd < 0) error_and_exit("Error en el open\n");

  while (1) {
    // Este bucle se ejecuta muchas veces por segundo
    int pos;
    if (read(0, &pos, sizeof(int)) < 0) error_and_exit("Error en el read\n");

    if (lseek(fd, pos, SEEK_SET) < 0) error_and_exit("Error en el lseek\n");

    char c;
    if (read(fd, &c, sizeof(char)) < 0) error_and_exit("Error en el read\n");

    sprintf(s, "Hijo2[id=%02d pid=%5d]: Caracter leido: %c\n", id, getpid (), c);
    write(1, s, strlen(s));
  }
}
