#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void error_y_exit(char *msg, int exit_status) {
  perror(msg);
  exit(exit_status);
}



int main(int argc, char *argv[]) {
  for (int i = 0; i < 4; ++i) {
    int pid = fork();
    if (pid < 0) error_y_exit("Error en fork", 1);
    if (pid == 0) {
      switch(i) {
        case 0:
          execlp("../S2/listaParametros", "listaParametros", "a", "b", (char*)NULL);
          error_y_exit("Ha fallado la mutación a listaParametros", 1);
          break;
        case 1:
          execlp("../S2/listaParametros", "listaParametros", (char*)NULL);
          error_y_exit("Ha fallado la mutación a listaParametros", 1);
          break;
        case 2:
          execlp("../S2/listaParametros", "listaParametros", "25", "4", (char*)NULL);
          error_y_exit("Ha fallado la mutación a listaParametros", 1);
          break;
        case 3:
          execlp("../S2/listaParametros", "listaParametros", "1024", "hola", "adios", (char*)NULL);
          error_y_exit("Ha fallado la mutación a listaParametros", 1);
          break;
      }
      exit(0);
    }
  }
  waitpid(-1, NULL, 0);
  return 0;
}
