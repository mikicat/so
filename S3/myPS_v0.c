#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void error_y_exit(char *msg, int exit_status) {
  perror(msg);
  exit(exit_status);
}

int main(int argc, char *argv[]) {
  char buff[80];
  sprintf(buff, "PID Padre: %d\n", getpid());
  write(1, buff, strlen(buff));
  if (argc == 2) {
    int pid = fork();
    if (pid < 0) error_y_exit("Error en fork", 1);
    else if (pid == 0) {
      sprintf(buff, "PID Hijo: %d\n", getpid());
      write(1, buff, strlen(buff));
      sprintf(buff, "Parámetro: %s\n", argv[1]);
      write(1, buff, strlen(buff));
    }
  }
  while(1) {};
  return 0;
}
