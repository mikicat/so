#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void error_y_exit(char *msg, int exit_status) {
  perror(msg);
  exit(exit_status);
}

void muta_a_PS(char *username) {
  execlp("ps", "ps", "-u", (char*)NULL);
  error_y_exit("Ha fallado la mutación al ps", 1);
}

int main(int argc, char *argv[]) {
  char buff[80];
  sprintf(buff, "PID Padre: %d\n", getpid());
  write(1, buff, strlen(buff));
  if (argc > 1) {
    for (int i = 1; i < argc; ++i) {
      int pid = fork();
      if (pid < 0) error_y_exit("Error en fork", 1);
      else if (pid == 0) {
        sprintf(buff, "PID Hijo: %d\n", getpid());
        write(1, buff, strlen(buff));
        sprintf(buff, "Parámetro: %s\n", argv[i]);
        write(1, buff, strlen(buff));
        //muta_a_PS(argv[1]);
        exit(0);
      }
    }
    while(waitpid(-1, NULL, 0) > 0);
    char c;
    read(0,&c,sizeof(char));
  }
  return 0;
}
