// Usage
    void usage()
    {
        char buf[256];
        sprintf (buf, "Usage: nombre_programa arg1 [arg2..argn]\n
        Este programa escribe por su salida la lista de argumentos que recibe\n");
        write(1, buf, strlen (buf));
    }

// Ejecución fork
    int ret = fork();
    if (ret == 0) {
        //hijo
    } else if (ret < 0) {
        //error
    } else {
        //padre
    }

// Esquema secuencial
    int n_proc = N;
    for (int i = 0; i < n_proc; ++i) {
        int ret = fork();
        if (ret < 0) trat_error();
        if (ret == 0) {
            //hijo
            codigo_hijo();
            exit(0);
        }
        waitpid(-1,NULL,0);
    }

//Esquema concurrente
    int n_proc = N;
    for (int i = 0; i < n_proc; ++i) {
        int ret = fork();
        if (ret < 0) trat_error();
        if (ret == 0) {
            //hijo
            codigo_hijo();
            exit(0);
        }
    }
    while (waitpid(-1,NULL,0) > 0);

// Trata exit_code
    #include <sys/wait.h>
    #include <string.h>
    void trataExitCode(int pid, int exit_code)
    {
        char buffer[128];
        if (WIFEXITED(exit_code)) {
            int statcode = WEXITSTATUS(exit_code);
            sprintf(buffer, "El proceso %d termian con exit code %d\n", pid, statcode);
        }
        else {
            int signcode = WTERMSIG(exit_code);
            sprintf(buffer, "El proceso %d termina por el signal %d\n", pid, signcode);
        }
        write(1, buffer, strlen(buffer));
    }

// Signals
    int kill(int pid, int signum);

    struct sigaction {
        sa_handler = {SIG_IGN, SIG_DFL, nombre_funcion(int s)};
        sa_mask = {signals bloqueados durante el tratamiento};
        sa_flags = {SA_RESETHAND, SA_RESTART};
    };
    int sigaction(int signum, struct sigaction *tratamiento,
                    struct sigaction *tratamiento_antiguo);

    int sigsuspend(sigset_t *mask);
    
// Manipulacion de mascaras
int sigemptyset(sigset_t *mask);
int sigfillset(sigset_t *mask);
int sigaddset(sigset_t *mask, int signum);
int sigdelset(sigset_t *mask, int signum);
int sigismember(sigset_t *mask, int signum); 

int sigprocmask(int operacion, sigset_t *mask, sigset_t *old_mask);
    operacion = {SIG_BLOCK, SIG_UNBLOCK, SIG_SETMASK}