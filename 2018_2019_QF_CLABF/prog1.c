#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <signal.h>

void usage() {
  char buff[256];
  sprintf(buff, "Usage: prog1 n\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void error_and_exit(char *msg) {
  perror(msg);
  exit(1);
}

void handle_signals(int s) {
  
}

int main(int argc, char *argv[]) {
  if (argc != 2) usage();
  int n = atoi(argv[1]);
  int *pids = malloc(sizeof(int)*n);

  struct sigaction sa;
  sa.sa_handler = &handle_signals;
  sa.sa_flags = 0;
  sigfillset(&sa.sa_mask);
  sigaction(SIGUSR1, &sa, NULL);

  for (int i = 0; i < n; ++i) {
    int ret = fork();
    if (ret == 0) {
      pids[i] = getpid();
      sigset_t mask;
      sigfillset(&mask);
      sigdelset(&mask, SIGUSR1);
      sigsuspend(&mask);
      char buff[30];
      sprintf(buff, "%d: USR1 recibido\n", pids[i]);
      write(1, buff, strlen(buff));
      exit(0);
    }
  }
  char c;
  read(0, &c, 1);
  for (int i = 0; i < n; ++i) {
    kill(pids[i], SIGUSR1);
  }
  free(pids);
  while (waitpid(-1, NULL, 0) > 0);
}
