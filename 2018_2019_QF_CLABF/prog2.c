#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

void usage() {
  char buff[30];
  sprintf(buff, "Usage: prog2 NAMED_PIPE num_children\n");
  write(1, buff, strlen(buff));
  exit(0);
}

void error_and_exit(char *msg) {
  perror(msg);
  exit(1);
}

int main(int argc, char *argv[]) {
  if (argc != 3) usage();
  int n = atoi(argv[2]);

  int fd[2];
  if (mknod(argv[1], S_IFIFO | 0666, 0) < 0) {
    if (errno != EEXIST) {
      error_and_exit("Error al crear la pipe\n");
    }
  }
  fd[0] = open(argv[1], O_RDONLY);
  fd[1] = open(argv[1], O_WRONLY);
  // pipe(fd);

  for (int i = 1; i <= n; ++i) {
    int ret = fork();
    if (ret < 0) error_and_exit("Error en el fork");
    else if (ret == 0) {
      dup2(fd[1], 1);
      close(fd[1]);
      close(fd[0]);
      char *secs;
      sprintf(secs, "%d", i);
      execlp("./aux", "aux", secs, (char *)NULL);
      error_and_exit("Error en el execlp");
    }
  }
  char c;
  close(fd[1]);
  while (read(fd[0], &c, 1) > 0) {
    write(1, &c, 1);
  }
  close(fd[0]);
  int pid;
  while ((pid = waitpid(-1, NULL, 0)) > 0) {
    char buff[15];
    sprintf(buff, "%d\n", pid);
    write(1, buff, strlen(buff));
  }
}
